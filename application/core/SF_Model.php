<?php
/**
 * Surefire Core Model
 *
 * @package     SurePush
 * @copyright   Copyright (c) 2015, Surefire Social, An Entity of GenNext Media, Inc. All Rights Reserved.
 * @subpackage  Model
 * @category    Libraries
 * @author      Surefire Social
 * @link        http://www.surefiresocial.com
 */
class SF_Model extends CI_Model {
    /**
     * Name of the database table
     * @var String
     */
    public $table_name;

    /**
     * Directory of Cache Files
     * @var String
     */
    public $cache_dir;

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        $this->cache_dir = APPPATH.'/cache/';
    }

    /**
     * Add or Edit (update) a record
     * @param Array $data array of fields
     */
    public function add_edit_record($data = NULL) {
        $id = isset($data['id']) ? $data['id'] : FALSE;
        if($data) {
            if($id) {
                $this->db->where('id', $id);
                $this->db->update($this->table_name, $data);
            } else {
                $this->db->insert($this->table_name, $data);
                $id = $this->db->insert_id();
            }
        }
        return $id;
    }

    /**
     * Set the active state of a record.
     * @param Integer $id     Record ID
     * @param Integer $active 1 = Active, 0 = Inactive
     */
    public function set_record_active_state($id = NULL, $active = 1) {
        $ret = TRUE;

        if($id) {
            $this->db->set('active', $active);
            $this->db->where('id', $id);
            $this->db->update($this->table_name);
        } else {
            $ret = FALSE;
        }

        return $ret;
    }

    /**
     * Get a count of all records
     * @return Integer Number of records
     */
    public function get_count($active = NULL) {
        if($active !== NULL)
            $this->db->where('active', $active);
        return $this->db->count_all_results($this->table_name);
    }

    /**
     * Get all records
     * @return Object All records data
     */
    public function get_records($active = NULL) {
        if($active !== NULL)
            $this->db->where('active', $active);
        $query = $this->db->get($this->table_name);

        return $query->result();
    }

    /**
     * Gets a specific record
     * @param  Integer/Array $id ID or list of the record(s) to retrieve
     * @return Object            Record data or List of Records
     */
    public function get_record($id = 0) {
        $ret = NULL;

        if (is_array($id)) {
            $this->db->where_in('id', $id);
            $query = $this->db->get($this->table_name);
            $ret   =  $query->result();
        } if($id !== 0) {
            $this->db->where('id', $id);
            $query = $this->db->get($this->table_name);
            $ret   = $query->row();
        }

        return $ret;        
    }

    /**
     * Removes a record
     * @param  Integer $id Record ID
     */
    public function remove_record($id = 0) {
        $this->db->where('id', $id);
        $this->db->delete($this->table_name);
    }
}