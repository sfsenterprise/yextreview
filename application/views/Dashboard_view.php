<div class="row">
    <div class="col-md-6 col-sm-6 clearfix">
        <h2><i style="color:#21a9e1" class="fa fa-sitemap"></i>  Budget Blinds Locations</h2>
    </div>
    <div class="col-md-3 col-sm-3 clearfix">
        <div id="actionStatus" class="alert alert-success"></div>
    </div>
    <div class="col-md-3 col-sm-3 clearfix">

        <a id="addBtn" href="<?php echo site_url("dashboard/add_edit_location"); ?>" class="btn btn-blue btn-icon pull-right headerButton" type="button">
            <span>Add New </span>
            <i class="entypo-plus"></i>
        </a>

    </div>
</div>
<table class="table table-bordered datatable" id="table-1">
    <thead>
        <tr>
            <th>#Id</th>
            <th>Location ID</th>
            <th>Location Name</th>
            <th>Script</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
<?php
if(isset($bbLocations)):  
    $i = 1;
    foreach ($bbLocations as $location):

?>
        <tr class="odd gradeX">
            <td><?php echo $i; ?></td>
            <td><?php echo $location->lication_id; ?></td>
            <td><?php echo $location->location_name; ?></td>
            <td><input type="text" style="width: 100%;" value='<?php echo $location->yext_script; ?>'></td>
            <td>
                <a href="<?php echo site_url('dashboard/add_edit_location/'.$location->id); ?>" class="btn btn-green btn-sm btn-icon icon-left editInfo">
                    <i class="entypo-pencil"></i>
                    Edit
                </a>
                &nbsp;&nbsp;&nbsp;
<!--
                <button data-delete-id="<?php echo $location->id; ?>" class="btn btn-danger btn-sm btn-icon icon-left deleteInfo">
                    <i class="entypo-trash"></i>
                    Archive
                </button>
-->

            </td>
        </tr>
<?php
    $i++;
    endforeach;
endif;
?>
    </tbody>
    <tfoot>
        <tr>
            <th>#Id</th>
            <th>Location ID</th>
            <th>Location Name</th>
            <th>Script</th>
            <th>Action</th>
        </tr>
    </tfoot>
</table>
