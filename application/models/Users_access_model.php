<?php
/**
 * Users Access Model
 *
 * @package     SurePush
 * @copyright   Copyright (c) 2015, Surefire Social, An Entity of GenNext Media, Inc. All Rights Reserved.
 * @subpackage  Model
 * @category    Libraries
 * @author      Surefire Social
 * @link        http://www.surefiresocial.com
 */

class Users_access_model extends SF_Model {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();
        $this->table_name = 'users_access';
    }

    public function get_access_id_by_user_id($user_id) {
        $this->db->select('access_id');
        $this->db->where('users_id', $user_id);
        $query = $this->db->get($this->table_name);

        $ret = array();
        foreach($query->result() as $result) {
            $ret[] = $result->access_id;
        }
        return $ret;
    }


    public function remove_users_access($user_id){
        $this->db->where('users_id', $user_id);
        $this->db->delete($this->table_name);  
    }
}