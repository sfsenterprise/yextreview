<?php
/**
 * Affiliate Model
 *
 * @package     SurePush
 * @copyright   Copyright (c) 2015, Surefire Social, An Entity of GenNext Media, Inc. All Rights Reserved.
 * @subpackage  Model
 * @category    Libraries
 * @author      Surefire Social
 * @link        http://www.surefiresocial.com
 */

class Yext_script_model extends SF_Model {
    public function __construct() {
        parent::__construct();

        $this->table_name = 'yext_script';
    }

    /**
     * Get all records
     * @return Object All records data
     */
    
    public function get_script_by_location_id($locationId = NULL, $active = 1) {
        if($active !== NULL)
            $this->db->where('active', $active);
        $this->db->where('lication_id', $locationId);
        $query = $this->db->get($this->table_name);

        return $query->row();
    }


}
