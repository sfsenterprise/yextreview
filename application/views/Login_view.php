<div class="login-container">
	<div class="login-header login-caret">
		<div class="login-content" style="width:600px;">
			<a id="logoContainer" href="javascript:void(0)" class="logo">
				<img src="<?php echo base_url();?>img/surefire-local.png" width="280" title="Surefire Local" alt="Surefire Local" />
				<strong>Budget Blinds Review API</strong>
			</a>

			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>logging in...</span>
			</div>
		</div>
	</div>

	<div class="login-progressbar">
		<div></div>
	</div>

	<div class="login-form">
		<div class="login-content">
			<div class="form-login-error">
				<h3>Invalid login</h3>
				<p>Invalid <strong>User</strong> / <strong>Password</strong> Try Again!</p>
			</div>

			<form method="post" role="form" id="form_login">
				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						<input type="text" class="form-control" name="username" id="username" placeholder="Username" autocomplete="off" />
					</div>
				</div>

				<div class="form-group">
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>
						<input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
					</div>
					<input type="hidden" id="check" name="check" value="<?php echo md5(time()); ?>" />
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						Login
					</button>
				</div>
			</form>

			<div class="login-bottom-links">
				<a href="auth/forget_password" class="link">Forgot your password?</a>
			</div>
		</div>
	</div>
</div>
