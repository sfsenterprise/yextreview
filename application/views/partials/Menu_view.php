        <div class="sidebar-menu fixed">
            <div class="sidebar-menu-inner">
                <header class="logo-env">
                    <!-- logo -->
                    <div class="logo">
                        <a href="<?php echo site_url("dashboard"); ?>">
                            <img src="<?php echo base_url();?>img/budget-blinds-logo-dt.svg" width="120" title="Budget Blinds" alt="budget-blinds" />
                        </a>
                    </div>
                    <!-- logo collapse icon -->
                    <div class="sidebar-collapse">
                        <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                            <i class="entypo-menu"></i>
                        </a>
                    </div>

                    <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                    <div class="sidebar-mobile-menu visible-xs">
                        <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                            <i class="entypo-menu"></i>
                        </a>
                    </div>
                </header>

                <ul id="main-menu" class="main-menu">
                    <li class="<?php if($menu == 'dashboard') echo 'active'?>">
                        <a href="<?php echo site_url("dashboard"); ?>">
                            <i class="entypo-gauge"></i>
                            <span class="title">Locations</span>
                        </a>
                    </li>

                </ul>

                <div id="poweredBy">
                    <ul class="main-menu">
                        <li class="logo">
                            <b>Powered By:</b><br/>
                            <img src="<?php echo base_url();?>img/SureFireSocial_Logo.png" width="120" alt="" />
                        </li>
                    </ul>
                </div>

            </div>
        </div>

        <div class="main-content">
            <div class="row">
                <!-- Profile Info and Notifications -->
                <div class="col-md-6 col-sm-8 clearfix">
                    <h3>Budget Blinds 1st Party Review</h3>
                    <i><?php echo date('F d, Y')?></i>
                </div>

                <!-- Raw Links -->
                <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                    <ul class="list-inline links-list pull-right">
                        <li class="sep"></li>
                        <li><b><?php echo $user->username;?></b></br>
                            <a href="<?php echo site_url("auth/logout"); ?>">
                                Log Out <i class="fa fa-sign-out"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <hr />
