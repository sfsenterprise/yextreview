<?php
/**
 * Auth Model
 *
 * @package     RBA Credit Application.
 * @copyright   Copyright (c) 2015, Surefire Social, An Entity of GenNext Media, Inc. All Rights Reserved.
 * @subpackage  Model
 * @category    Libraries
 * @author      Surefire Social
 * @link        http://www.surefiresocial.com
 */

class Auth_model extends SF_Model {
    public function __construct() {
        parent::__construct();

        $this->table_name = 'users';
    }

    public function login($params = array()){

		$ret = NULL;

        if($params['username'] && $params['password']) {
        	$this -> db -> select('id, username, role');
		    $this -> db -> where('username', $params['username']);
		    $this -> db -> where('password', md5($params['password']));
    
            $query =  $this-> db ->get($this->table_name);
            $ret   =  $query->row();
        }
        
        return $ret;
    }
}