<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Sure Fire Social ADPI Control Panel" />
    <meta name="author" content="" />

    <title><?php echo $page_title; ?></title>

    <link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui-1.10.3.custom.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/entypo.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.css">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/style.css">
<?php
    if($css_includes):
    foreach ($css_includes as $css):
?>
        <link rel="stylesheet" href="<?php echo site_url('/css/'.$css.'.css'); ?>">
<?php
    endforeach;
    endif;
?>
    <script src="<?php echo base_url();?>js/jquery-1.11.0.min.js"></script>
    <script>
        $.noConflict();
        var baseurl = "<?php echo site_url(''); ?>"
        var resourceurl = "http://adpi-resources.s3-website-us-east-1.amazonaws.com/"
    </script>

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="page-body <?php echo isset($body_classes) ? $body_classes : ''; ?>" >
    <div class="page-container">