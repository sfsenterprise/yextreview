<div class="office-office">
    <div class="first-layer">
        <img class="dentist-img" src="https://s3.amazonaws.com/adpi-resources/images/<?php echo $profile_image; ?>">
        <div class="about-office">
            <div class="office-name"><?php echo str_replace(' ,', ',',$dentist_last.', '.$dentist_first.' '.$dentist_middle.', '.$dentist_degree); ?><span><?php echo $dentist_title; ?></span></div>
            <div class="office-address">
                <?php echo $address1; ?><?php echo (trim($address2) != '') ? ', '.$address2 : ''; ?>
                </br>
                <?php echo $city; ?>, <?php echo $state; ?> <?php echo $postal_code; ?>
            </div>
            <div class="office-phone"><?php echo nl2br($phone_primary); ?></div>
        </div>
        <div class="away-from-you">
            <div class="map-indicator" data-lat="<?php echo $lat; ?>" data-lng="<?php echo $lng; ?>"><?php echo $count; ?></div>
            <div class="miles"><?php echo round((($distance * 100) / 100), 2); ?> mi</div>
        </div>
    </div>
    <div class="second-layer panel-group">
        <div class="office-hour">
            <a href="/about-us/our-dentists/<?php echo $dentist_slug; ?>/" class="collapsed">Dentist Bio</a>
        </div>
        <div class="services-office">
            <a class="collapsed" href="https://maps.google.com?saddr=Current+Location&daddr=<?php echo $address1; ?>+<?php echo $address2; ?>+<?php echo $city; ?>+<?php echo $state; ?>+<?php echo $postal_code; ?>" target = "_blank">Get Directions</a>
        </div>
        <div class="Request-appt">
<?php if(isset($dentist_zocdoc) && !empty($dentist_zocdoc)): ?>
            <a href="<?php echo $dentist_zocdoc; ?>" target="_blank" class="collapsed zocdoc_button request-an-appointment-dsearch" onclick="ga('send', 'pageview', 'ReqApptSearchDent');">
                Schedule an Appt.
            </a>
<?php else: ?>
            <a  class="collapsed set_practice request-an-appointment-dsearch" data-practice-id="<?php echo $name; ?>" href="javascript:void(0)" onclick="ga('send', 'pageview', 'ReqApptSearchDent');">
                Request an Appt.
            </a>
<?php endif; ?>
        </div>
    </div>
</div>
