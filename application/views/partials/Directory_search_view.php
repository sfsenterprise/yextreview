            <div class="row pd-x2 <?php echo $styleName; ?>"> <!-- // row start -->
              <div class="col-md-4">
                <h4><a href="<?php echo $url; ?>" target="_blank"><?php echo $name; ?></a></h4>
                <div class="locationRating">
                     <span>Overall Customer Rating</span>
                     <div class="stars js-stars header-stars">
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                      <i class="fa fa-star" aria-hidden="true"></i>
                     </div>
                  </div>
                  <span><?php echo $address.", ".$address_2; ?> <br>
                  <span><?php echo $city.", ".$state." ".$postal_code;?></span></span>
              </div>

              <div class="col-md-4">
                <a target="_blank" href="https://maps.google.com?saddr=Current+Location&daddr=<?php echo $address; ?>+<?php echo $address_2; ?>+<?php echo $city; ?>+<?php echo $state; ?>+<?php echo $postal_code; ?>" class="btn"><i class="fa fa-share-square" aria-hidden="true"></i> Get Directions</a>
                <div class="clear"></div>
                <span>Phone:</span>
                  <div class="locationPhoneNumber"><a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></div>
                  <span>Website:</span>
                  <div class="locationWebUrl"><a href="<?php echo $url; ?>" target="_blank"><?php echo $url; ?></a></div>

              </div>

<?php 


    $dateName = date("N"); 
    $isOpen = 'Not Available'; 
    if($dateName == 1 && isset($hours['Monday']) && $hours['Monday'][0]!='' && $hours['Monday'][1]!=''){
        $date2  = DateTime::createFromFormat('H:i a', $hours['Monday'][0]);
        $date3  = DateTime::createFromFormat('H:i a', $hours['Monday'][1]);
        
        $tz   = new DateTimeZone($email);
        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $current = DateTime::createFromFormat('H:i a', $current_time);
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        //$isOpen = 'Now Open';
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }elseif($dateName == 2 && isset($hours['Tuesday']) && $hours['Tuesday'][0]!='' && $hours['Tuesday'][1]!=''){
        $date2 = DateTime::createFromFormat('H:i a', $hours['Tuesday'][0]);
        $date3 = DateTime::createFromFormat('H:i a', $hours['Tuesday'][1]);

        $tz = new DateTimeZone($email);

        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }elseif($dateName == 3 && isset($hours['Wednesday']) && $hours['Wednesday'][0]!='' && $hours['Wednesday'][1]!=''){
        $date2 = DateTime::createFromFormat('H:i a', $hours['Wednesday'][0]);
        $date3 = DateTime::createFromFormat('H:i a', $hours['Wednesday'][1]);

        $tz = new DateTimeZone($email);

        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }elseif($dateName == 4 && isset($hours['Thursday']) && $hours['Thursday'][0]!='' && $hours['Thursday'][1]!=''){
        $date2 = DateTime::createFromFormat('H:i a', $hours['Thursday'][0]);
        $date3 = DateTime::createFromFormat('H:i a', $hours['Thursday'][1]);

        $tz = new DateTimeZone($email);

        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }elseif($dateName == 5 && isset($hours['Friday']) && $hours['Friday'][0]!='' && $hours['Friday'][1]!=''){
        $date2 = DateTime::createFromFormat('H:i a', $hours['Friday'][0]);
        $date3 = DateTime::createFromFormat('H:i a', $hours['Friday'][1]);

        $tz = new DateTimeZone($email);

        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }elseif($dateName == 6 && isset($hours['Saturday']) && $hours['Saturday'][0]!='' && $hours['Saturday'][1]!=''){
        $date2 = DateTime::createFromFormat('H:i a', $hours['Saturday'][0]);
        $date3 = DateTime::createFromFormat('H:i a', $hours['Saturday'][1]);

        $tz = new DateTimeZone($email);

        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }elseif($dateName == 7 && isset($hours['Sunday']) && $hours['Sunday'][0]!='' && $hours['Sunday'][1]!=''){
        $date2 = DateTime::createFromFormat('H:i a', $hours['Sunday'][0]);
        $date3 = DateTime::createFromFormat('H:i a', $hours['Sunday'][1]);

        $tz = new DateTimeZone($email);

        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }elseif($dateName == 7 && isset($hours['Tuesday']) && $hours['Tuesday'][0]!='' && $hours['Tuesday'][1]!=''){
        $date2 = DateTime::createFromFormat('H:i a', $hours['Tuesday'][0]);
        $date3 = DateTime::createFromFormat('H:i a', $hours['Tuesday'][1]);

        //$usersTimezone = 'America/New_York';
        $tz = new DateTimeZone($email);

        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }

?>                 
                
              <div class="col-md-4">
                <div class="row">
                  <div class="col-md-2">Hours</div>
                  <div class="col-md-10">
                    <a href="javascript:void(0)" class="btn"><?php echo $isOpen; ?></a>
                    <div class="timeContainer">
                      <ul>
                        <li>Monday ---- 
                            <span><?php if(isset($hours['Monday'])){
                                echo $hours['Monday'][0].' - '.$hours['Monday'][1];
                             } else {
                               echo "Closed";
                             }?></span>
                          </li>
                        <li>Tuesday --- <span>
         <?php if(isset($hours['Tuesday'])){
            echo $hours['Tuesday'][0].' - '.$hours['Tuesday'][1];
         } else {
           echo "Closed";
         }?></span></li>
                        <li>Wednesday ---- <span>
         <?php if(isset($hours['Wednesday'])){
            echo $hours['Wednesday'][0].' - '.$hours['Wednesday'][1];
         } else {
           echo "Closed";
         }?></span></li>
                        <li>Thursday ---- <span>
         <?php if(isset($hours['Thursday'])){
            echo $hours['Thursday'][0].' - '.$hours['Thursday'][1];
         } else {
           echo "Closed";
         }?></span></li>
                        <li>Friday ----- <span>
         <?php if(isset($hours['Friday'])){
            echo $hours['Friday'][0].' - '.$hours['Friday'][1];
         } else {
           echo "Closed";
         }?></span></li>
                        <li>Saturday ---- <span>
                             <?php if(isset($hours['Saturday'])){
                                echo $hours['Saturday'][0].' - '.$hours['Saturday'][1];
                             } else {
                               echo "Closed";
                             }?></span></li>
                        <li>Sunday ----- <span>
                             <?php if(isset($hours['Sunday'])){
                                echo $hours['Sunday'][0].' - '.$hours['Sunday'][1];
                             } else {
                               echo "Closed";
                             }?></span>
                          </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>

            </div> <!-- // row end -->
