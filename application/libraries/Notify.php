<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notify {

	function __construct()
	{
		$this->ci =& get_instance();
		$config = Array(
          'protocol' => 'smtp',
          //'smtp_host' => 'smtp.sendgrid.net',
          'smtp_host' => '108.168.190.109',
          'smtp_user' => 'surefire-admin',
          'smtp_pass' => 'a94ebbd9970f68359009b355a9be4dF3',
          'smtp_port' => 587,
          'mailtype' => "html",
          'crlf' => "\r\n",
          'newline' => "\r\n"
		);
		$this->ci->load->library('email', $config);
	}

	function new_admin_email($to='',$password='',$from="info@adpi.com"){
        $this->ci->email->from($from, 'ADPI');
        $this->ci->email->to($to);
        $this->ci->email->subject('ADPI Email Test 2');
        $this->ci->email->message('Testing the email class.');
        $this->ci->email->send();
        echo $this->ci->email->print_debugger();
	}

	function send_forger_password_email($to, $customer_text){
 		$retValue = false;
 		$subject  = "ADPI Control Panel Password Reset";
 		$text     = $customer_text;
 		$from     = 'no-reply@adpi.com';

        $this->ci->email->from($from, 'ADPI');
        $this->ci->email->to($to);
        $this->ci->email->subject($subject);
        $this->ci->email->message($text);

	    if($this->ci->email->send()){
	    	$retValue = true;
	    }
	    return $retValue;
	}

}
