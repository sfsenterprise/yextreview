<?php
/**
 * user Controller
 */
class User extends SF_Controller {
    public function __construct() {
        parent::__construct();

        $this->data['page_title'] = 'ADPI - User Admin';
        $this->data['menu']       = 'adminUser';
        if($this->data['user']->role  != "S" && $this->data['user']->role  != "E"){
            redirect(site_url('auth/logout'));
        }
    }

    public function index() {
        $this->data['css_includes'][] = 'datatables.responsive';
        $this->data['css_includes'][] = 'select2-bootstrap';
        $this->data['css_includes'][] = 'select2';

        $this->data['js_includes'][]  = 'jquery.dataTables.min';
        $this->data['js_includes'][]  = 'TableTools.min';
        $this->data['js_includes'][]  = 'dataTables.bootstrap';
        $this->data['js_includes'][]  = 'jquery.dataTables.columnFilter';
        $this->data['js_includes'][]  = 'lodash.min';
        $this->data['js_includes'][]  = 'datatables.responsive';
        $this->data['js_includes'][]  = 'select2.min';
		$this->data['js_includes'][]  = 'pages/users';

        // $this->load->library('Notify');
        // $this->notify->new_admin_email();

    	$this->data['users']          = $this->User->get_records(true);
        $this->display('Userlist');
    }

    public function add_edit_user($id=NULL){
	    $this->data['js_includes'][]  = 'pages/users-details';
	    $this->load->library('Sort_obj');

	    $this->data['affiliates'] = $this->sort_obj->sort_object($this->Affiliate->get_records(true));

        if($this->security->xss_clean($this->input->post('check'))) {
        	$userAccess = [];
            $fields     = ['id','first_name','last_name','username','role'];
            foreach($fields as $field) {
                $data[$field] = $this->security->xss_clean($this->input->post($field));
            }

            /*if New User OR if password == comfirm password*/
            if(($this->input->post('id') == 0) || ($this->input->post('password') != '' && $this->input->post('password') == $this->input->post('confirm')))
            	$data['password'] = $this->_hash_password($this->input->post('password'));

            /*if User role is Affiliate Editor*/
            if($this->input->post('role') == 'A')
                $userAccess    = $this->security->xss_clean($this->input->post('affiliate'));

            /*if User role is Practice Editor*/
            if($this->input->post('role') == 'P' && !is_null($this->input->post('practice')))
                $userAccess    = $this->security->xss_clean($this->input->post('practice'));

            $id  =  $this->_save_update($data, $userAccess);
        }

        if($id) {
            $this->data['userInfo'] =  $this->User->get_record($id);

            /*if User role is Affiliate Editor*/
            if($this->data['userInfo']->role == 'A'){
            	$this->data['userAffiliatesId'] = $this->Users_access->get_access_id_by_user_id($this->data['userInfo']->id);
            }
            /*if User role is Practice Editor
              Get All Practice Ids From User_access Table
              Then Get All Affiliate Id By Practice Id.
            */
            if($this->data['userInfo']->role == 'P'){
              $this->data['userPracticeId']     = $this->Users_access->get_access_id_by_user_id($this->data['userInfo']->id);
              $this->data['userAffiliatesId']   = $this->Practice->get_affiliate_ids_by_practice_ids($this->data['userPracticeId']);

              /*Regenerate All Practice List*/
              foreach ($this->data['userAffiliatesId'] as $value) {
                    $this->data['practiceList']['practice_'.$value] = $this->Practice->get_practice_by_affiliate($value);
              }
            }
        }

	    $this->display('User');
    }

    /**
    * Ajax calls
    */
    public function remove_user() {
        $user_id  =  $this->input->post('id');

        $this->db->trans_start();
        $this->Users_access->remove_users_access($user_id);
        $this->User->remove_record($user_id);
        $this->db->trans_complete();

        echo json_encode($this->db->trans_status());
    }

    public function count_user($id = 0){
        $username  = $this->input->post('username');
        $userCount = $this->User->get_count_user_email($username, $id);

        if($userCount == 0)
            echo 'true';
        else
            echo 'false';
    }

    /**
     * Private calls
     */
    private function _save_update($data = [], $access = []){
            $user_id = $this->User->add_edit_record($data);
            $this->Users_access->remove_users_access($user_id);

            foreach ($access as $key => $value) {
                $users_access = [
                        'users_id'  => $user_id,
                        'access_id' => $value,
                    ];

            	$this->Users_access->add_edit_record($users_access);
            }
            return $user_id;
    }

    private function _hash_password($password = NULL) {
        return $password ? md5($password.$this->config->item('encryption_key')) : NULL;
    }
}
