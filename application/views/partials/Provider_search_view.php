<div class="row listLocation <?php echo $styleName; ?>">
   <div class="col-md-4 firstHalf">
     <h6><a href="<?php echo $url; ?>" target="_blank"><?php echo $name; ?></a></h6>
     <div class="locationRating">
       <span>Overall Customer Rating</span>
       <div class="stars js-stars header-stars">
         <svg class="yext-star yext-reviews-star full" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="display: inline;">    <title>Full Star</title> <desc>Created with Sketch.</desc> <defs></defs> <g stroke="none" fill-rule="evenodd"> <path class="yext-star-fill full" d="M15.75 6.375L12.26 9.78l.826 4.807c.01.068.01.126.01.193 0 .25-.116.481-.394.481a.783.783 0 0 1-.385-.115l-4.318-2.27-4.317 2.27a.813.813 0 0 1-.384.115c-.279 0-.404-.231-.404-.48 0-.068.01-.126.019-.194L3.74 9.78.24 6.375C.125 6.25 0 6.086 0 5.914c0-.288.298-.404.538-.443l4.827-.701L7.529.394c.086-.182.25-.394.47-.394.222 0 .385.212.471.394l2.164 4.376 4.827.701c.231.039.538.155.538.443 0 .172-.125.336-.25.461"></path> </g> </svg>
         <svg class="yext-star yext-reviews-star full" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="display: inline;">    <title>Full Star</title> <desc>Created with Sketch.</desc> <defs></defs> <g stroke="none" fill-rule="evenodd"> <path class="yext-star-fill full" d="M15.75 6.375L12.26 9.78l.826 4.807c.01.068.01.126.01.193 0 .25-.116.481-.394.481a.783.783 0 0 1-.385-.115l-4.318-2.27-4.317 2.27a.813.813 0 0 1-.384.115c-.279 0-.404-.231-.404-.48 0-.068.01-.126.019-.194L3.74 9.78.24 6.375C.125 6.25 0 6.086 0 5.914c0-.288.298-.404.538-.443l4.827-.701L7.529.394c.086-.182.25-.394.47-.394.222 0 .385.212.471.394l2.164 4.376 4.827.701c.231.039.538.155.538.443 0 .172-.125.336-.25.461"></path> </g> </svg>
         <svg class="yext-star yext-reviews-star full" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="display: inline;">    <title>Full Star</title> <desc>Created with Sketch.</desc> <defs></defs> <g stroke="none" fill-rule="evenodd"> <path class="yext-star-fill full" d="M15.75 6.375L12.26 9.78l.826 4.807c.01.068.01.126.01.193 0 .25-.116.481-.394.481a.783.783 0 0 1-.385-.115l-4.318-2.27-4.317 2.27a.813.813 0 0 1-.384.115c-.279 0-.404-.231-.404-.48 0-.068.01-.126.019-.194L3.74 9.78.24 6.375C.125 6.25 0 6.086 0 5.914c0-.288.298-.404.538-.443l4.827-.701L7.529.394c.086-.182.25-.394.47-.394.222 0 .385.212.471.394l2.164 4.376 4.827.701c.231.039.538.155.538.443 0 .172-.125.336-.25.461"></path> </g> </svg>
         <svg class="yext-star yext-reviews-star full" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="display: inline;">    <title>Full Star</title> <desc>Created with Sketch.</desc> <defs></defs> <g stroke="none" fill-rule="evenodd"> <path class="yext-star-fill full" d="M15.75 6.375L12.26 9.78l.826 4.807c.01.068.01.126.01.193 0 .25-.116.481-.394.481a.783.783 0 0 1-.385-.115l-4.318-2.27-4.317 2.27a.813.813 0 0 1-.384.115c-.279 0-.404-.231-.404-.48 0-.068.01-.126.019-.194L3.74 9.78.24 6.375C.125 6.25 0 6.086 0 5.914c0-.288.298-.404.538-.443l4.827-.701L7.529.394c.086-.182.25-.394.47-.394.222 0 .385.212.471.394l2.164 4.376 4.827.701c.231.039.538.155.538.443 0 .172-.125.336-.25.461"></path> </g> </svg>
         <svg class="yext-star yext-reviews-star full" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="display: inline;">    <title>Full Star</title> <desc>Created with Sketch.</desc> <defs></defs> <g stroke="none" fill-rule="evenodd"> <path class="yext-star-fill full" d="M15.75 6.375L12.26 9.78l.826 4.807c.01.068.01.126.01.193 0 .25-.116.481-.394.481a.783.783 0 0 1-.385-.115l-4.318-2.27-4.317 2.27a.813.813 0 0 1-.384.115c-.279 0-.404-.231-.404-.48 0-.068.01-.126.019-.194L3.74 9.78.24 6.375C.125 6.25 0 6.086 0 5.914c0-.288.298-.404.538-.443l4.827-.701L7.529.394c.086-.182.25-.394.47-.394.222 0 .385.212.471.394l2.164 4.376 4.827.701c.231.039.538.155.538.443 0 .172-.125.336-.25.461"></path> </g> </svg> 
       </div>
     </div>
     <span><?php echo $address.", ".$address_2; ?></spam></br>
     <span><?php echo $city.", ".$state." ".$postal_code;?></spam>
   </div>
   <div class="col-md-4 centerPatr">
     <a target="_blank" href="https://maps.google.com?saddr=Current+Location&daddr=<?php echo $address; ?>+<?php echo $address_2; ?>+<?php echo $city; ?>+<?php echo $state; ?>+<?php echo $postal_code; ?>" class="directionLink"><i class="fa fa-share-square" aria-hidden="true"></i> Get Directions</a>
     <span>Phone:</span>
     <div class="locationPhoneNumber"><a href="tel:+<?php echo $phone; ?>" ><?php echo $phone; ?></a></div>
     <span>Website:</span>
     <div class="locationWebUrl"><a href="<?php echo $url; ?>" target="_blank"><?php echo $url; ?></a></div>
   </div>
<?php 
//function checkOpenTime($openTime, $closeTime, $timeZone){
//    $date2 = DateTime::createFromFormat('H:i a', $openTime);
//    $date3 = DateTime::createFromFormat('H:i a', $closeTime); 
//    $tz    = new DateTimeZone($timeZone);
//}

    $dateName = date("N"); 
    $isOpen = 'Not Available'; 
    if($dateName == 1 && isset($hours['Monday']) && $hours['Monday'][0]!='' && $hours['Monday'][1]!=''){
        $date2  = DateTime::createFromFormat('H:i a', $hours['Monday'][0]);
        $date3  = DateTime::createFromFormat('H:i a', $hours['Monday'][1]);
        
        $tz   = new DateTimeZone($email);
        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $current = DateTime::createFromFormat('H:i a', $current_time);
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        //$isOpen = 'Now Open';
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }elseif($dateName == 2 && isset($hours['Tuesday']) && $hours['Tuesday'][0]!='' && $hours['Tuesday'][1]!=''){
        $date2 = DateTime::createFromFormat('H:i a', $hours['Tuesday'][0]);
        $date3 = DateTime::createFromFormat('H:i a', $hours['Tuesday'][1]);

        $tz = new DateTimeZone($email);

        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }elseif($dateName == 3 && isset($hours['Wednesday']) && $hours['Wednesday'][0]!='' && $hours['Wednesday'][1]!=''){
        $date2 = DateTime::createFromFormat('H:i a', $hours['Wednesday'][0]);
        $date3 = DateTime::createFromFormat('H:i a', $hours['Wednesday'][1]);

        $tz = new DateTimeZone($email);

        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }elseif($dateName == 4 && isset($hours['Thursday']) && $hours['Thursday'][0]!='' && $hours['Thursday'][1]!=''){
        $date2 = DateTime::createFromFormat('H:i a', $hours['Thursday'][0]);
        $date3 = DateTime::createFromFormat('H:i a', $hours['Thursday'][1]);

        $tz = new DateTimeZone($email);

        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }elseif($dateName == 5 && isset($hours['Friday']) && $hours['Friday'][0]!='' && $hours['Friday'][1]!=''){
        $date2 = DateTime::createFromFormat('H:i a', $hours['Friday'][0]);
        $date3 = DateTime::createFromFormat('H:i a', $hours['Friday'][1]);

        $tz = new DateTimeZone($email);

        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }elseif($dateName == 6 && isset($hours['Saturday']) && $hours['Saturday'][0]!='' && $hours['Saturday'][1]!=''){
        $date2 = DateTime::createFromFormat('H:i a', $hours['Saturday'][0]);
        $date3 = DateTime::createFromFormat('H:i a', $hours['Saturday'][1]);

        $tz = new DateTimeZone($email);

        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }elseif($dateName == 7 && isset($hours['Sunday']) && $hours['Sunday'][0]!='' && $hours['Sunday'][1]!=''){
        $date2 = DateTime::createFromFormat('H:i a', $hours['Sunday'][0]);
        $date3 = DateTime::createFromFormat('H:i a', $hours['Sunday'][1]);

        $tz = new DateTimeZone($email);

        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }elseif($dateName == 7 && isset($hours['Tuesday']) && $hours['Tuesday'][0]!='' && $hours['Tuesday'][1]!=''){
        $date2 = DateTime::createFromFormat('H:i a', $hours['Tuesday'][0]);
        $date3 = DateTime::createFromFormat('H:i a', $hours['Tuesday'][1]);

        //$usersTimezone = 'America/New_York';
        $tz = new DateTimeZone($email);

        $date = new DateTime("now",$tz);
        $current_time = $date->format('h:i a');
        $date1 = DateTime::createFromFormat('H:i a', $current_time);
        if ($date1 > $date2 && $date1 < $date3){
            $isOpen = 'Now Open';
        }else{
            $isOpen = 'Now Closed'; 
        }
    }

?>    
   <div class="col-md-4 lastHalf">
     <div class="hoursContainer">Hours</div>
     <div class="hoursDetaild">
       <a href="javascript:void(0)"><?php echo $isOpen; ?></a>
       <div class="timeContainer">
         <ul>
       <li>Monday ---- <span>
         <?php if(isset($hours['Monday'])){
            echo $hours['Monday'][0].' - '.$hours['Monday'][1];
         } else {
           echo "Closed";
         }?></span></li>
       <li>Tuesday --- <span>
         <?php if(isset($hours['Tuesday'])){
            echo $hours['Tuesday'][0].' - '.$hours['Tuesday'][1];
         } else {
           echo "Closed";
         }?></span></li>
       <li>Wednesday ---- <span>
         <?php if(isset($hours['Wednesday'])){
            echo $hours['Wednesday'][0].' - '.$hours['Wednesday'][1];
         } else {
           echo "Closed";
         }?></span></li>
       <li>Thursday ---- <span>
         <?php if(isset($hours['Thursday'])){
            echo $hours['Thursday'][0].' - '.$hours['Thursday'][1];
         } else {
           echo "Closed";
         }?></span></li>
       <li>Friday ----- <span>
         <?php if(isset($hours['Friday'])){
            echo $hours['Friday'][0].' - '.$hours['Friday'][1];
         } else {
           echo "Closed";
         }?></span></li>
       <li>Saturday ---- <span>
         <?php if(isset($hours['Saturday'])){
            echo $hours['Saturday'][0].' - '.$hours['Saturday'][1];
         } else {
           echo "Closed";
         }?></span></li>
       <li>Sunday ----- <span>
         <?php if(isset($hours['Sunday'])){
            echo $hours['Sunday'][0].' - '.$hours['Sunday'][1];
         } else {
           echo "Closed";
         }?></span></li>
     </ul>
       </div>
     </div>
   </div>
</div>
