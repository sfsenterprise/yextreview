<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sort_obj {

	function __construct()
	{
		$this->ci =& get_instance();
	}

	function sort_object($object)
	{
        usort($object, function($a, $b)
        {
            return strcmp($a->name, $b->name);
        });

        return $object; 
	}
    
    function checkOpenTime($openTime, $closeTime, $timeZone){
        $date2  = DateTime::createFromFormat('H:i a', $openTime);
        $date3  = DateTime::createFromFormat('H:i a', $closeTime); 
        $tz     = new DateTimeZone($timeZone);
        $isOpen = 'Close';    
        $date = new DateTime("now",$tz);
            $current_time = $date->format('h:i a');

            $date1 = DateTime::createFromFormat('H:i a', $current_time);
            if ($date1 > $date2 && $date1 < $date3){
                $isOpen = 'Open';
            } 
        return $isOpen; 
    }

}