<?php
/**
 * Auth (Default) Controller
 */
class Auth extends SF_Controller {
    public function __construct() {
        parent::__construct(FALSE);
    }

    public function index() {
    	$this->data['page_title']     = 'Budget Blinds 1st Party Reviews';
    	$this->data['body_classes']   = 'login-page login-form-fall';
    	$this->data['js_includes'][]  = 'pages/login';
        $this->display('Login', FALSE);
    }

    public function logout() {
        $this->session->unset_userdata('user');
        redirect(site_url('auth'));
    }

    /**
     * Ajax Calls
     */

    public function login() {
        $this->data['username'] = $this->input->post('username');
        $this->data['password'] = $this->input->post('password');

        $response = [];
        if($this->input->post('check')) {
            if($this->data['username'] && $this->data['password']) {
                $user = $this->User->get_record_by_credentials($this->data['username'], $this->_hash_password($this->data['password']));

                if($user) {
                    if($user->role == 'A'){
                       $user->role_affiliates_ids = $this->Users_access->get_access_id_by_user_id($user->id);
                    }
                    else if($user->role == 'P'){
                       $user->role_practice_ids   = $this->Users_access->get_access_id_by_user_id($user->id);
                       $user->role_affiliates_ids = $this->Practice->get_affiliate_ids_by_practice_ids($user->role_practice_ids);
                    }

                    $this->session->set_userdata('user', $user);
                    $response['login_status'] = 'success';
                    $response['redirect_url'] = '';
                } else {
                    $response['error']        = 'Username and password are not correct. Please check your credentials.';
                    $response['login_status'] = 'invalid';
                }
            } else {
                $response['error']        = 'Username and password are required.';
                $response['login_status'] = 'invalid';
            }
        }

        echo json_encode($response);
    }

    public function forget_password() {
        //$this->data['page_title']     = 'Sales Portal | Login';
        $this->data['body_classes']   = 'login-page login-form-fall';
        $this->data['js_includes'][]  = 'pages/forget_pass';
        $this->display('forget', FALSE);
    }

    public function update_password() {
        $username  = strtolower($this->input->post('username'));
        $userInfo = $this->User->get_user_by_email($username);

        if($userInfo && $userInfo->active ==1){
            $this->load->helper('string');
            $temp_password = random_string('alnum', 16);
            $token = random_string('alnum', 80);

            $data = array(
                    'id'            =>  $userInfo->id,
                    'temp_password' =>  $this->_hash_password($temp_password),
                    'token'         =>  $token
                    );
            $userId = $this->User->add_edit_record($data);
            if($userId){
                $this->load->library('Notify'); // Load Notify Lebrary
                $emailbody['token']     = $token;
                $emailbody['password']  = $temp_password;
                $customer_text          = $this->load->view('email/forget_pass_email_view', $emailbody, TRUE);
                $this->notify->send_forger_password_email($userInfo->username, $customer_text);
            }

            $response['login_status']  = 'success';
        }else{
            $response['login_status'] = 'invalid';
        }
        echo json_encode($response);
    }

    public function reset_passowrd($token) {
       $this->data['page_title']    = 'Sales Portal | Login';
       $this->data['body_classes']  = 'login-page login-form-fall';
       $this->data['js_includes'][] = 'pages/reset-pass';

       $token = trim($token);
       $userInfo = $this->User->get_user_by_token($token);
       if($userInfo){
        $this->data['token'] = $token;
       }else{
        $this->data['token'] = NULL;
       }
       $this->display('reset_password', FALSE);
    }

    public function change_password() {
        $temp_password  = $this->input->post('temp_password');
        $password       = $this->input->post('password');
        $token          = $this->input->post('token');

        $userInfo = $this->User->get_user_by_token_password($token, $this->_hash_password($temp_password));
        if($userInfo){
             $data = array(
                    'id'            =>  $userInfo->id,
                    'password'      =>  $this->_hash_password($password),
                    'temp_password' =>  '',
                    'token'         =>  ''
                    );
            $userId = $this->User->add_edit_record($data);
            if($userId) {


               $response['login_status']  = 'success';
            }else{
                $response['login_status'] = 'invalid';
            }
        }else{
            $response['login_status'] = 'invalid';
        }
        echo json_encode($response);
    }

    /**
     * Private calls
     */

    private function _hash_password($password = NULL) {
        $ret = NULL;
        if($password) {
            $ret = md5($password.$this->config->item('encryption_key'));
        }
        return $ret;
    }
}
