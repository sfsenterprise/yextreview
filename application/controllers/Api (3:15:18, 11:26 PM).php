<?php
require_once APPPATH.'/libraries/REST_Controller.php';

/* ==============================================================
   == Main API functions                                       ==
   == -------------------------------------------------------- ==
   == @author SureFire Social                                  ==
   == @version 1.0                                             ==
   == -------------------------------------------------------- ==
   == This implements a RESTful addtion to CodeIgniter         ==
   == Documentation can be found at:                           ==
   == https://github.com/chriskacerguis/codeigniter-restserver ==
   ============================================================== */



class Api extends REST_Controller {

    public function __construct() {
        parent::__construct();
        // Add contruction
    }

    /* ================================
       ==== Public Calls ==============
       ================================ */
    public function index_get() {
        $this->response(array('status' => '0', 'message' => 'Default Message'));
    }

    public function rba_locations_get($lat = NULL, $lng = NULL) {
      $api_response = [
              'status'       => -1,
              'message'      => 'Error in data.',
              'lat'          => $lat,
              'lng'          => $lng,
          ];

      if($lat && $lng) {
          $location_sql = 'select *, ( 3959 * acos( cos( radians( '.$lat.' ) ) * cos( radians( lat ) ) * cos( radians( lng ) - '.
                                      'radians( '.$lng.' ) ) + sin( radians( '.$lat.' ) ) * sin( radians( lat ) ) ) ) AS distance '.
                                      'FROM nap a WHERE a.active=1 ORDER BY distance';

          $locations = $this->db->query($location_sql);
          $locations = $locations->result();

          foreach ($locations as $location) {
              $location->hours    = unserialize($location->hours);
          }

          // $location_list = [];
          // $counter = 0;
          // $old_lat = 0;
          // $old_lng = 0;
          // foreach($practices as $practice) {
          //     if($practice->services || $all) {
          //         $practice_list[] = $practice;
          //         if($practice->lat != $old_lat && $practice->lng != $old_lng) {
          //             $counter++;
          //         }
          //
          //         if($practice->practice_img == '')
          //             $practice->practice_img = $default_img;
          //         $practice->count = $counter;
          //         $return_html .= $this->load->view('partials/Provider_search_view', $practice, TRUE);
          //         $old_lat = $practice->lat;
          //         $old_lng = $practice->lng;
          //     }
          // }

          $api_response = [
                  'status'  => 1,
                  'message' => "Success",
                  'html'    => $locations,
                  'lat'     => $lat,
                  'lng'     => $lng,
              ];
      }

      $this->response($api_response);
    }

    public function practice_locations_get($affiliate_id = NULL, $lat = NULL, $lng = NULL, $distance = 50, $default_img = NULL, $services = NULL) {
        $all          = $services == '' ? TRUE : FALSE;
        $return_html  = '';
        $services     = $services ? urldecode($services) : NULL;
        $api_response = [
                'status'       => -1,
                'message'      => 'Error in data.',
                'affiliate_id' => $affiliate_id,
                'lat'          => $lat,
                'lng'          => $lng,
            ];

        if($affiliate_id && $lat && $lng) {
            $practice_by_location_sql = 'select *, ( 3959 * acos( cos( radians( '.$lat.' ) ) * cos( radians( lat ) ) * cos( radians( lng ) - '.
                                        'radians( '.$lng.' ) ) + sin( radians( '.$lat.' ) ) * sin( radians( lat ) ) ) ) AS distance '.
                                        'FROM practices a WHERE a.affiliate_id = '.$affiliate_id.' and a.active=1'.
                                        ($distance > 0 ? ' HAVING distance < '.$distance : '').
                                        ' ORDER BY distance';

            $practices = $this->db->query($practice_by_location_sql);
            $practices = $practices->result();

            foreach ($practices as $practice) {
                $practice->services = $this->_get_services_by_practice($practice->id, $services);
                $practice->hours    = unserialize($practice->hours);
            }

            $services = explode(',', urldecode($services));

            $practice_list = [];
            $counter = 0;
            $old_lat = 0;
            $old_lng = 0;
            foreach($practices as $practice) {
                if($practice->services || $all) {
                    $practice_list[] = $practice;
                    if($practice->lat != $old_lat && $practice->lng != $old_lng) {
                        $counter++;
                    }

                    if($practice->practice_img == '')
                        $practice->practice_img = $default_img;
                    $practice->count = $counter;
                    $return_html .= $this->load->view('partials/Provider_search_view', $practice, TRUE);
                    $old_lat = $practice->lat;
                    $old_lng = $practice->lng;
                }
            }

            $api_response = [
                    'status'  => 1,
                    'message' => $practice_list,
                    'html'    => $return_html,
                    'lat'     => $lat,
                    'lng'     => $lng,
                ];
        }

        $this->response($api_response);
    }

    public function dentist_locations_get($affiliate_id = NULL, $lat = NULL, $lng = NULL, $distance = 50, $default_img = NULL, $services = NULL) {
        $return_html  = '';
        $services     = $services ? urldecode($services) : NULL;

        $api_response = [
                'status'       => -1,
                'message'      => 'Error in data.',
                'affiliate_id' => $affiliate_id,
                'lat'          => $lat,
                'lng'          => $lng,
            ];

        if($affiliate_id && $lat && $lng) {
            $dentist_by_location_sql = 'select d.id as dentist_id, d.name as dentist_name, d.first_name as dentist_first, d.middle_name as dentist_middle, d.last_name as dentist_last, d.degree as dentist_degree, d.title as dentist_title, d.profile_image, d.zocdoc_url as dentist_zocdoc, d.slug as dentist_slug, p.*,'.
                                        '( 3959 * acos( cos( radians( '.$lat.' ) ) * cos( radians( lat ) ) * cos( radians( lng ) - '.
                                        'radians( '.$lng.' ) ) + sin( radians( '.$lat.' ) ) * sin( radians( lat ) ) ) ) AS distance '.
                                        'FROM practices p, practice_dentists pd, dentists d '.
                                        'WHERE p.affiliate_id = '.$affiliate_id.' and p.active=1 and pd.practice_id = p.id and pd.dentist_id = d.id and d.active=1'.
                                        ($distance > 0 ? ' HAVING distance < '.$distance : '').
                                        ' ORDER BY distance';

            $dentists = $this->db->query($dentist_by_location_sql);
            $dentists = $dentists->result();

            $dentist_list = [];
            $counter = 0;
            $old_lat = 0;
            $old_lng = 0;
            foreach ($dentists as $dentist) {
                $has_service    = $this->_get_services_by_dentist($dentist->dentist_id, $services);
                if($has_service) {
                    $dentist_list[] = $dentist;
                    if($dentist->lat != $old_lat && $dentist->lng != $old_lng) {
                        $counter++;
                    }

                    if($dentist->profile_image == '')
                        $dentist->profile_image = $default_img;
                    $dentist->count = $counter;
                    $return_html .= $this->load->view('partials/Dentist_search_view', $dentist, TRUE);
                    $old_lat = $dentist->lat;
                    $old_lng = $dentist->lng;
                }
            }

            $api_response = [
                    'status'  => 1,
                    'message' => $dentist_list,
                    'html'    => $return_html,
                    'lat'     => $lat,
                    'lng'     => $lng,
                ];
        }

        $this->response($api_response);
    }



    /* ================================
       ==== Private Calls =============
       ================================ */

    public function _make_api_call($url, $call = 'test', $type = 'GET', $values = array(), $controller='adpi') {
        $api_url = $url.'/api/'.$controller.'.'.$call;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER,         0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL,            $api_url);
        curl_setopt($curl, CURLOPT_USERAGENT,      "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        if($values) {
            $query_values = array();
            foreach($values as $k => $v) $query_values[] = $k.'='.urlencode($v);
            $query_string = implode('&', $query_values);

            if(strtoupper($type) == 'POST') {
                curl_setopt($curl, CURLOPT_POST, count($query_values));
                curl_setopt($curl, CURLOPT_POSTFIELDS, $query_string);
            } else {
                curl_setopt($curl, CURLOPT_URL, $api_url.'?'.$query_string);
            }
        }

        $curl_response = curl_exec($curl);

        curl_close($curl);
        return json_decode($curl_response);
    }
}
