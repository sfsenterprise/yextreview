<?php if(isset($dentist_bio_header)):?>
<?php foreach ($dentist_bio_header as $bio_header): ?>                            
     <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                 <label class="control-label" for="professional_statement"> <strong> <?php echo $bio_header->name; ?>: </strong></label>
                 <textarea  rows="3" name="boi_info[<?php echo $bio_header->id; ?>][text]" class="medium form-control"><?php echo $bio_header->text; ?></textarea>
                 <input type="hidden" name="boi_info[<?php echo $bio_header->id; ?>][id]" value="<?php echo $bio_header->boi_id; ?>">
            </div>
        </div>
    </div>
<?php endforeach; ?>
<?php endif; ?>  