<div class="login-container">

	<div class="login-header login-caret">
		<div class="login-content" style="width:600px;">
			<a id="logoContainer" href="javascript:void(0)" class="logo">
				<img src="<?php echo base_url();?>img/logo.jpg" width="220" alt="Renewal by Andersen The Best Windows" />
				<strong>Control Panel</strong>
				<p class="description">Enter your email, and we will send the reset link.</p>
			</a>

			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>logging in...</span>
			</div>
		</div>
	</div>


	<div class="login-progressbar">
		<div></div>
	</div>

	<div class="login-form">

		<div class="login-content">

			<form method="post" role="form" id="form_forgot_password">

				<div class="form-forgotpassword-success">
					<i class="entypo-check"></i>
					<h3>Reset email has been sent.</h3>
					<p>Please check your email, reset password link will expire in 24 hours.</p>
				</div>

				<div class="form-login-error">
					<h3>Invalids</h3>
					<p>Invalid <strong>User Email</strong> Try Again!</p>
				</div>

				<div class="form-steps">

					<div id="forgetPass">

						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">
									<i class="entypo-mail"></i>
								</div>

								<input type="text" class="form-control" name="username" id="username" placeholder="Email" data-mask="email" autocomplete="off" />
							</div>
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block btn-login">
								<i class="entypo-right-open-mini"></i>
								Submit
							</button>
							<input type="hidden" id="check" name="check" value="<?php echo md5(time()); ?>" />
						</div>

					</div>

				</div>

			</form>


			<div class="login-bottom-links">

				<a href="<?php echo site_url("auth"); ?>" class="link">
					<i class="entypo-lock"></i>
					Return to Login Page
				</a>



			</div>

		</div>

	</div>

</div>
