<?php
require_once APPPATH.'/third_party/aws/aws-autoloader.php';

use Aws\S3\Exception\S3Exception;

class Aws {
    private $sdk;
    private $s3;
    private $bucket;
    private $public_url;

    public function __construct() {
        $config = [
            'version'     => 'latest',
            'region'      => 'us-east-1',
            'credentials' => array(
                    'key'     => 'AKIAJJKNUYCKDVGC2E5Q',
                    'secret'  => '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'
                )
        ];

        $this->sdk        = new Aws\Sdk($config);
        $this->s3         = $this->sdk->creates3();

        $this->bucket     = 'adpi-resources';
        $this->public_url = 'https://s3.amazonaws.com/adpi-resources/images/';
    }

    public function get_s3_content($key) {
        return $this->s3->getObject([
            'Bucket' => $this->bucket,
            'Key'    => 'images/'.$key
        ]);
    }

    public function put_s3_content($file = NULL) {
        $ret = array(
                'status'  => 0,
                'message' => 'File not sent.'
            );

        if($file) {
            // Get file extension
            $tmp = explode('.', $file['name']);
            $ext = strtolower(end($tmp));
            $key = md5(microtime()).'.'.$ext;

            try {
                $this->s3->putObject([
                    'Bucket' => $this->bucket,
                    'Key'    => 'images/'.$key,
                    'Body'   => file_get_contents($file['tmp_name']),
                    'ACL'    => 'public-read'
                ]);

                $ret = array(
                        'staus'   => 1,
                        'message' => 'File saved to AWS S3.',
                        'key'     => $key,
                        'url'     => $this->public_url.$key,
                    );
            } catch(S3Exception $e) {
                $ret['message'] = 'Trouble sending fiel to AWS S3.';
            }

        }

        return $ret;
    }

    public function delete_s3_content($key) {
        return $this->s3->deleteObject(array(
            'Bucket' => $this->bucket,
            'Key'    => 'images/'.$key
        ));
    }
}
