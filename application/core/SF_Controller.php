<?php
/**
 * Sure Fire Core Controller
 *
 * @package     SurePush
 * @copyright   Copyright (c) 2015, Surefire Social, An Entity of GenNext Media, Inc. All Rights Reserved.
 * @subpackage  Controller
 * @category    Libraries
 * @author      Surefire Social
 * @link        http://www.surefiresocial.com
 */
class SF_Controller extends CI_Controller {
    /**
     * List of data elements used in all controllers and passed to views.
     * @var Array
     */
    public $data;

    /**
     * Constructor
     * @param Boolean $require_login Set weather login is required to view pages in this controller
     */
    public function __construct($require_login = TRUE) {
        parent::__construct();
        $this->data = array(
            'error' => NULL,
            'warn'  => NULL,
            'info'  => NULL,
            'debug' => NULL
        );

        $this->data['ip']            = $_SERVER['REMOTE_ADDR'];
        $this->data['user']          = $this->session->user;
        $this->data['show_debug']    = $this->config->item('sp_debug');

        $this->data['page_title']    = 'ADPI';

        $this->data['body_classes']  = '';
        $this->data['menu']          = '';
        $this->data['submenu']       = '';
        $this->data['css_includes']  = array();
        $this->data['js_includes']   = array();

        if($require_login) {
            if(!$this->data['user']) {
                redirect(site_url('auth'));
            }
        }

    }

    /**
     * Display - outputs templates to form complete html page
     * @param  String  $page    Name of the view to output. If $page is not passed, only the header and footer are displayed.
     * @param  Boolean $sidebar Ste TRUE if you want a sidebar to be displayed, otherwise FALSE
     */
    public function display($page = FALSE, $sidebar = TRUE) {
        $this->load->view('partials/Header_view', $this->data);

        if($sidebar)
            $this->load->view('partials/Menu_view',   $this->data);
        else
            $this->load->view('partials/Nomenu_view',   $this->data);

        if($page)
            $this->load->view($page.'_view',      $this->data);

        $this->load->view('partials/Footer_view', $this->data);
    }

    public function clear_cache($url){
        $var_domain        = preg_replace('#^https?://#', '', rtrim($url, "/"));
        $var_cache_servers = $this->config->item('cache_servers'); //array('nginx.c1.e.node.sfs.io','nginx.c1.w.node.sfs.io','nginx.c1.tokyo.node.sfs.io');

        $username          = $this->config->item('cache_server_username'); //'spapidev'
        $password          = $this->config->item('cache_server_password'); //'rE3q8uAeIadcMGtVMknizqdO3uVL5Pcz'
        $response          = []; 

        foreach($var_cache_servers as $value){
            $URL  = $value.'/cache_clear?domain='.$var_domain;

            $ch   = curl_init();
            curl_setopt($ch, CURLOPT_URL,$URL);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_USERAGENT, 'SFS-Nginx-Bot');
            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
            $result      = curl_exec ($ch);
            curl_close ($ch);

            $data        = @unserialize($result);
            if($data !== false) {
                if(is_numeric($data[0])){
                    $response[] = '$data[0] Cleared';
                }else{
                // Must be a message
                    $response[] = '$data[0] Failed';
                }
            }
            return $response;
        }
    }

    public function api_call($url, $call = 'test', $type = 'GET', $values = array(), $controller='adpi') {
        $api_url = $url.'/api/'.$controller.'.'.$call;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER,         0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL,            $api_url);
        curl_setopt($curl, CURLOPT_USERAGENT,      "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        if($values) {
            $query_values = array();
            foreach($values as $k => $v) $query_values[] = $k.'='.urlencode($v);
            $query_string = implode('&', $query_values);

            if(strtoupper($type) == 'POST') {
                curl_setopt($curl, CURLOPT_POST, count($query_values));
                curl_setopt($curl, CURLOPT_POSTFIELDS, $query_string);
            } else {
                curl_setopt($curl, CURLOPT_URL, $api_url.'?'.$query_string);
            }
        }

        $curl_response = curl_exec($curl);

        curl_close($curl);
        return json_decode($curl_response);
    }

}
