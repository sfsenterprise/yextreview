<div class="login-container">

	<div class="login-header login-caret">
		<div class="login-content" style="width:600px;">
			<a id="logoContainer" href="javascript:void(0)" class="logo">
				<img src="<?php echo base_url();?>img/logo.jpg" width="220" alt="Renewal by Andersen The Best Windows" />
				<strong>Control Panel</strong>
				<p class="description"></p>
			</a>

			<!-- progress bar indicator -->
			<div class="login-progressbar-indicator">
				<h3>43%</h3>
				<span>logging in...</span>
			</div>
		</div>
	</div>

	<div class="login-progressbar">
		<div></div>
	</div>

	<div class="login-form">

		<div class="login-content">

			<div class="form-login-error">
				<h3>Invalid Temporary Password</h3>
				<p>Please Check Your Email For <strong>Temporary Password</strong> & Try Again!</p>
			</div>
			<div class="form-forgotpassword-success">
					<i class="entypo-check"></i>
					<h3>Password Update Successful.</h3>
				</div>

			<?php if(is_null($token)): ?>
				<div class="form-login-error" style="display:block;">
					<h3>Invalid Token</h3>
					<p>Please Check Your Email For Link With Token & Try Again!</p>
				</div>

	 	    <?php else: ?>
				<form method="post" role="form" id="form_login">
					<div class="form-group">

						<div class="input-group">
							<div class="input-group-addon">
								<i class="entypo-key"></i>
							</div>
							<input type="password" class="form-control" name="temp_password" id="temp_password" placeholder="Temporary Password" autocomplete="off" />
						</div>

					</div>

					<div class="form-group">

						<div class="input-group">
							<div class="input-group-addon">
								<i class="entypo-key"></i>
							</div>

							<input type="password" class="form-control" name="password" id="password" placeholder="New Password" autocomplete="off" />
						</div>
						<input type="hidden" id="token" name="token" value="<?php echo $token; ?>" />
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block btn-login">
							<i class="entypo-login"></i>
							Reset Password
						</button>
					</div>
				</form>
			<?php endif; ?>

			<div class="login-bottom-links">

				<a href="<?php echo site_url("auth/forget_password"); ?>" class="link">Forgot Password?</a>
				&nbsp;&nbsp; | &nbsp;&nbsp;
				<a href="<?php echo site_url("auth"); ?>" class="link"><i class="entypo-lock"></i>Return to Login</a>

			</div>

		</div>

	</div>

</div>
