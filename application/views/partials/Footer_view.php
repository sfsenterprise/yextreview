<?php

?>
        </div>
        <!-- Bottom scripts (common) -->
        <script src="<?php echo base_url();?>js/main-gsap.js"></script>
        <script src="<?php echo base_url();?>js/jquery-ui-1.10.3.minimal.min.js"></script>
       
        <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>js/joinable.js"></script>
        <script src="<?php echo base_url();?>js/resizeable.js"></script>
        <script src="<?php echo base_url();?>js/neon-api.js"></script>
        <script src="<?php echo base_url();?>js/jquery.validate.min.js"></script>
<?php
    if($js_includes): foreach ($js_includes as $include):
?>
        <script type="text/javascript" src="<?php echo base_url();?>js/<?php echo $include; ?>.js"></script>
<?php
    endforeach; endif;
?>
        <!-- JavaScripts initializations and stuff -->
        <script src="<?php echo base_url();?>js/neon-custom.js"></script>
<?php if(isset($tinymce)): ?>        
        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<?php endif; ?>        
    </div>
</body>
</html>