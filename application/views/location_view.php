<form role="form" method="post" class="form-groups-bordered validate" enctype="multipart/form-data" action="<?php site_url('dashboard/add_edit_location')?>">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <i style="color:#21a9e1" class="fa fa-sitemap"></i>
                        Budget Blinds Location
                    </div>

                    <div class="panel-options">
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="name"> <strong> BB Location Id:</strong></label>
                                    <input type="text" class="form-control" data-validate="required" id="lication_id" name="lication_id" value="<?php if(isset($bbLocation)) echo $bbLocation->lication_id; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="name"> <strong> BB Location Name:</strong></label>
                                    <input type="text" class="form-control" data-validate="required" id="location_name" name="location_name" value="<?php if(isset($bbLocation)) echo $bbLocation->location_name; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label" for="url"> <strong> Review Script:</strong></label>
                                    <input type="text" class="form-control" name="yext_script" id="yext_script"  value='<?php if(isset($bbLocation)) echo $bbLocation->yext_script; ?>'>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
            </div>
        </div>
    </div>



    <div class="form-group default-padding">
        <button type="submit" class="btn btn-success">Save</button>
        <input type="hidden" id="check" name="check" value="<?php echo md5(time()); ?>" />
        <input type="hidden" id="id" name="id" value="<?php if(isset($bbLocation)) echo $bbLocation->id; ?>" />
    </div>
</form>
