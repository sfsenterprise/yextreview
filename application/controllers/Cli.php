<?php

class Cli extends CI_Controller {
    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        // this Controller is CLI only
        if(!is_cli()) {
            redirect(site_url('dashboard'), 'refresh');
        }
    }

    /**
     * [get_coords_for_practice description]
     * @return [type] [description]
     */
    public function get_coords_for_practice() {
        $maps_api       = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
        $address_fields = ['address1','address2','city','state','postal_code'];

        $this->load->model('Practice_model', 'Practice');

        $practices = $this->Practice->get_records();

        foreach ($practices as $practice) {
            if(!$practice->lat || !$practice->lng) {
                $address_build  = [];

                foreach ($address_fields as $field) {
                    if(trim($practice->$field) != '')
                        $address_build[] = trim($practice->$field);
                }

                $address_lookup = implode(',', $address_build);
                $address_json   = file_get_contents($maps_api.urlencode($address_lookup));
                $address_json   = json_decode($address_json);

                $practice->lat = isset($address_json->results[0]->geometry->location->lat) ? $address_json->results[0]->geometry->location->lat : NULL;
                $practice->lng = isset($address_json->results[0]->geometry->location->lng) ? $address_json->results[0]->geometry->location->lng : NULL;

                if($practice->lat && $practice->lng) {
                    $this->Practice->add_edit_record((array)$practice);
                    echo $practice->name." Located\n";
                } else {
                    echo $practice->name.' Not Found ('.$maps_api.urlencode($address_lookup).")\n";
                }
            }
        }
    }

    /**
     * [create_dentist_page description]
     * @param  [type] $affiliate_id [description]
     * @return [type]               [description]
     */
    public function create_dentist_page($affiliate_id = NULL) {
        if($affiliate_id) {
            $dentists  = $this->Dentist->get_dentist_records_by_affiliate_ids([$affiliate_id]);
            $affiliate = $this->Affiliate->get_record($affiliate_id);
            $url       = $affiliate->url.'/api/adpi.';

            foreach ($dentists as $dentist) {
                $page_slug = preg_replace('/[^a-z\d ]+/i', '', $dentist->name);
                //$page_slug = strtolower(str_replace(' ', '-', $page_slug));
                $page_slug = strtolower(preg_replace('/\s+/', '-', $page_slug));

                $results = $this->_api_call($url.'get_page_details', 'post', ['page_slug' => '/about-us/our-dentists/'.$page_slug]);

                if($results->error == 1) {
                    $results = $this->_api_call($url.'insert_update_page', 'post', [
                        'title'         => $dentist->name,
                        'page_slug'     => $page_slug,
                        'parent_id'     => 190,
                        'page_template' => 'dentist'
                    ]);

                    $dentist->slug = $page_slug;
                    $this->Dentist->add_edit_record((array)$dentist);
                    echo $dentist->name." Page Added\n";
                } else {
                    echo $dentist->name." Page Found\n";
                }
            }
        }
    }

    public function restructure_name() {

        $dentists = $this->Dentist->get_records(true);

        foreach ($dentists as $dentist) {
            $exploded = explode(',', $dentist->name, 2);
            $name= $exploded[0];
            $degree = isset($exploded[1])? $exploded[1]:'';

            if (strpos($degree, 'Dr.') !== false){
                $name = '##'.$degree.' '.$name;
                $degree = '';
            }


            $parts = explode(' ', $name); // $meta->post_title
            $name_first = array_shift($parts);
            $name_last = array_pop($parts);
            $name_middle = trim(implode(' ', $parts));

            $data = array(
                    'id'           => $dentist->id,
                    'last_name'    => $name_last ,
                    'first_name'   => $name_first ,
                    'middle_name'  => $name_middle ,
                    'degree'       => $degree
            );
            $response = $this->Dentist->add_edit_record($data);
        }
        echo 'Completed..';
    }

    public function reviewid_bulkUpload(){
       $row = 1;
       $handle = fopen(APPPATH."third_party/ADPI_Dentist_ORMID_all.csv", "r");

       while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
          // $num = count($data);
         // echo "<p> $num fields in line $row: <br /></p>\n";

            $row++;
            $data = array(
                    'id'           => trim($data[0]),
                    'review_id'    => trim($data[1])
            );
            $response = $this->Dentist->add_edit_record($data);
       }

       fclose($handle);
       echo 'Completed..';
    }

    public function reviewid_practice_bulkUpload(){
       $row = 1;
       $handle = fopen(APPPATH."third_party/ADPI_Practice_OEM_List1.csv", "r");
       $empty_count=0;
       while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
           if(trim($data[0]) == '' && $data[1] == ''){
             $empty_count++;
             continue;
           }
           if($empty_count == 5){
               break;
           }

            $row++;
            $data = array(
                    'id'           => trim($data[0]),
                    'review_id'    => trim($data[1])
            );
            $response = $this->Practice->add_edit_record($data);
       }

       fclose($handle);
       echo 'Completed..';
    }

    /**
     * [create_practice_page description]
     * @param  [type] $affiliate_id [description]
     * @return [type]               [description]
     */
    public function create_practice_page($affiliate_id = NULL) {
        if($affiliate_id) {
            $practices = $this->Practice->get_practice_by_affiliate($affiliate_id);
            $affiliate = $this->Affiliate->get_record($affiliate_id);
            $url       = $affiliate->url.'/api/adpi.';

            foreach ($practices as $practice) {
                $page_slug = preg_replace('/[^a-z\d ]+/i', '', $practice->name);
                //$page_slug = strtolower(str_replace(' ', '-', $page_slug));
                $page_slug = strtolower(preg_replace('/\s+/', '-', $page_slug));

                $results = $this->_api_call($url.'get_page_details', 'post', ['page_slug' => '/practices/'.$page_slug]);

                if($results->error == 1) {
                    $results = $this->_api_call($url.'insert_update_page', 'post', [
                        'title'         => $practice->name,
                        'page_slug'     => $page_slug,
                        'parent_id'     => 381,
                        'page_template' => 'practice'
                    ]);

                    $practice->slug = $page_slug;
                    $this->Practice->add_edit_record((array)$practice);
                    echo $practice->name." Page Added\n";
                } else {
                    echo $practice->name." Page Found\n";
                }
            }
        }
    }

    /**
     * Setup Surepulse accounts for practices by affiliate
     * @param  string $affiliate_id Affiliate ID to set up practice accounts in  Surepulse
     * @return string               Message of call status
     */
    public function surepulse_setup($affiliate_id = NULL) {
        if($affiliate_id) {
            $surepulse_url = $this->config->item('surepusle_api_url').'accountadmin/newSPAccount';
            $affiliate     = $this->Affiliate->get_record($affiliate_id);
            $practices     = $this->Practice->get_practice_by_affiliate($affiliate_id);

            foreach ($practices as $practice) {
                $data = [
                    'name'          => $practice->name,
                    'url'           => $affiliate->url.'practices/'.$practice->slug,
                    'isMicrosite'   => 1,
                    'n2lMasterSite' => $affiliate->url,
                    'n2lRegion'     => $affiliate->name,
                    'gaUnder'       => $affiliate->url,
                ];

                $results = $this->_api_call($surepulse_url, 'post', $data);
                print_r($results);
                echo "-------------------------\n";
            }
        } else {
            echo 'ERROR: Missing Affiliate ID'."\n\n";
        }
    }



    /**
     * Setup Surepulse accounts for practices by Practice id
     * @param  string $practice_id Peactice ID to set up practice accounts in  Surepulse
     * @return string               Message of call status
     */
    public function surepulse_setup_by_practice_id($practice_id = NULL) {
        if($practice_id) {
            $surepulse_url = $this->config->item('surepusle_api_url').'accountadmin/newSPAccount';
            $practice      = $this->Practice->get_record($practice_id);
            $affiliate     = $this->Affiliate->get_record($practice->affiliate_id);

            $data = [
                'name'          => $practice->name,
                'url'           => $affiliate->url.'practices/'.$practice->slug,
                'isMicrosite'   => 1,
                'n2lMasterSite' => $affiliate->url,
                'n2lRegion'     => $affiliate->name,
                'gaUnder'       => $affiliate->url,
            ];

            //$results = $this->_api_call($surepulse_url, 'post', $data);
            print_r($data);
            echo "-------------------------\n";

        } else {
            echo 'ERROR: Missing Affiliate ID'."\n\n";
        }
    }
    /*****************
     * Private Calls *
     *****************/
    /**
     * API Curl call funtion
     * @param  string $url    URL of API call
     * @param  string $type   Method of call: GET or POST
     * @param  array  $values Get or Post data
     * @return string         JSON Response
     */
    private function _api_call($url, $type = 'GET', $values = array()) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADER,         0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL,            $url);
        curl_setopt($curl, CURLOPT_USERAGENT,      "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        if($values) {
            $query_values = array();
            foreach($values as $k => $v) $query_values[] = $k.'='.urlencode($v);
            $query_string = implode('&', $query_values);

            if(strtoupper($type) == 'POST') {
                curl_setopt($curl, CURLOPT_POST, count($query_values));
                curl_setopt($curl, CURLOPT_POSTFIELDS, $query_string);
            } else {
                curl_setopt($curl, CURLOPT_URL, $api_url.'?'.$query_string);
            }
        }

        $curl_response = curl_exec($curl);

        curl_close($curl);
        return json_decode($curl_response);
    }
}
