<?php
/**
 * Main Controller
 */
class Dashboard extends SF_Controller {
    public function __construct() {
        parent::__construct();
        $this->data['page_title'] = 'Budget Blinds 1st Party Review';
    }

    public function index() {
        $this->data['menu']         = 'dashboard';
        $this->data['bbLocations'] = $this->YextScript->get_records(1);
        $this->display('Dashboard');
    }

     public function add_edit_location($id=NULL) {

       if($this->input->post('check')) {

           $data['lication_id']   = $this->security->xss_clean($this->input->post('lication_id'));
           $data['location_name'] = $this->input->post('location_name'); 
           $data['yext_script']   = $this->input->post('yext_script');
           $data['id']            = $this->security->xss_clean($this->input->post('id'));

           $id    = $this->YextScript->add_edit_record($data);
       }

       if($id) {
         $this->data['bbLocation'] = $this->YextScript->get_record($id);
       }
       $this->display('location');
     }
}
