<?php if(isset($practices)): 
   $perColum    = floor(count($practices)/2);
    if(count($practices) % 2 !==0)
		$perColum  = $perColum+1;
	$counter   = 0;  

    foreach($practices AS $practice): 
	    if($counter ==0 ){echo '<div class="col-md-6">';}
?>
    	<div class="color-blue" style="overflow:hidden;">
	    	<div class="indentStyle">
				<input type="checkbox"  style="margin-top: 0;" name="practice[]" class="req_practice" value="<?php echo $practice->id ?>" <?php if(isset($affiliate_services) && in_array($practice->id, explode(',', $affiliate_services[0]->id))) echo 'checked'; ?>  >
			</div>	
			<label class="labelIndent"><?php echo $practice->name.', '.$practice->address1.', '.$practice->city. ', '.$practice->state; ?></label>
		</div>
<?php   $counter++;
		if($counter == $perColum){ 
			echo '</div>'; 
			$counter = 0; 
		} 
		endforeach; 
	endif;
	if($counter != 0) echo '</div>';
?>