<?php
/**
 * User Model
 *
 * @package     SurePush
 * @copyright   Copyright (c) 2015, Surefire Social, An Entity of GenNext Media, Inc. All Rights Reserved.
 * @subpackage  Model
 * @category    Libraries
 * @author      Surefire Social
 * @link        http://www.surefiresocial.com
 */

class User_model extends SF_Model {
    public function __construct() {
        parent::__construct();

        $this->table_name = 'users';
    }

    public function get_record_by_credentials($username = NULL, $password = NULL) {
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $query = $this->db->get($this->table_name);

        return $query->row();
    }

    public function get_count_user_email($username, $uId=0) {
        $this->db->where('username', $username);
        if($uId !=0)
            $this->db->where('id !=', $uId);

        return $this->db->count_all_results($this->table_name);
    }

    public function get_user_by_token($token) {
        $this->db->where('token', $token);
        $query = $this->db->get($this->table_name);
        return $query->row();
    }

    public function get_user_by_token_password($token, $password) {
        $this->db->where('temp_password', $password);
        $this->db->where('token', $token);
        $query = $this->db->get($this->table_name);
        return $query->row();
    }

    public function get_user_by_email($email) {
        $this->db->where('username', $email);
        $query = $this->db->get($this->table_name);
        return $query->row();
    }
}
